﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp2.enums;

namespace ConsoleApp2.BaseClasses
{
    public abstract class Item
    {

        public string Name { get; set; }
        public int Level { get; set; }
        public EquipmentSlot EquipmentSlot { get; set; }

        public Item()
        {

        }
        public Item(string name, int level, EquipmentSlot equipmentSlot)
        {
            Name = name;
            Level = level;
            EquipmentSlot = equipmentSlot;
        }
    }
}
