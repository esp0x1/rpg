﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp2.Attributes;
using ConsoleApp2.enums;
using ConsoleApp2.Exceptions;
using ConsoleApp2.Interfaces;
using ConsoleApp2.SubClasses;

namespace ConsoleApp2.BaseClasses
{
    public abstract class Character : ICharacterActions
    {
        public string Name { get; private set; }
        public int Level { get; private set; }
        public BasePrimaryAttributes BasePrimaryAttributes { get; protected set; }
        public Dictionary<EquipmentSlot, Item> EquippedItems { get; protected set; }
        public BasePrimaryAttributes OnLevelUpAttributeIncrease { get; protected set; }
        public WeaponType[] AllowedWeapons { get; protected set; }
        public ArmourType[] AllowedArmour { get; protected set; }
        public AttributeType PrimaryAttributeType { get; protected set; }


        public Character(string name)
        {
            Level = 1;
            EquippedItems = new Dictionary<EquipmentSlot, Item>();
            Name = name;
        }
        /// <summary>
        /// Methods to equip items, two different param signatures using overloading to separate the responsibilities of each method
        /// </summary>
        /// <param name="weapon"></param> Param is weapon or armor
        /// <exception cref="InvalidWeaponException"></exception> throws exception if item is higher level than the character or it is not allowed to equip
        /// <returns> Success String or throws exception </returns>
        public string EquipItem(Weapon weapon)
        {
            if (!AllowedWeapons.Contains<WeaponType>(weapon.WeaponType) || Level < weapon.Level)
            {
                throw new InvalidWeaponException();
            }
            EquippedItems[weapon.EquipmentSlot] = weapon;
            return "New Weapon Equipped!";
        }
        public string EquipItem(Armor armor)
        {
            if (!AllowedArmour.Contains<ArmourType>(armor.ArmourType) || Level < armor.Level)
            {
                throw new InvalidArmourException();
            }
            EquippedItems[armor.EquipmentSlot] = armor;     
            return "New Armour Equipped!";
        }
        /// <summary>
        /// Method to show character information
        /// Create dictionary of type enum,int -> fill with baseAttributes + gearAttributes per AttributeType
        /// create stringbuilder, append values
        /// foreach KeyVal pair in dict print append a line to the SB with the key(name of attribute) and value(int)
        /// Write the string to console.
        /// </summary>
        public void CharacterInfo()
        {
            Dictionary<AttributeType, int> TotalAttributes = GetTotalPrimaryAttributes();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("         Character Information         ");
            sb.AppendLine("_______________________________________");
            sb.AppendLine($"    Name:                 {Name}");
            sb.AppendLine($"    Level:                {Level}");
            foreach (KeyValuePair<AttributeType, int> entry in TotalAttributes)
            {
                sb.AppendLine($"    {entry.Key}:             {entry.Value}");
            }
            sb.AppendLine($"    Damage:               {DoDamage()}");
            Console.WriteLine(sb);
        }
        /// <summary>
        /// helper method for DoDamage(); and CharacterInfo();
        /// Add baseAttributes of Character to Dict of AttributeType and Int
        /// for each item the character has equipped, increment the value of baseAttributes[AttrType] 
        /// </summary>
        /// <returns> dict containing AttrType and Int where int is the total base+gear attribute value </returns>
        private Dictionary<AttributeType, int> GetTotalPrimaryAttributes()
        {
            Dictionary<AttributeType, int> TotalAttributes = new Dictionary<AttributeType, int>
            {
                {AttributeType.Strength, BasePrimaryAttributes.Strength},
                {AttributeType.Dexterity, BasePrimaryAttributes.Dexterity},
                {AttributeType.Intelligence, BasePrimaryAttributes.Intelligence},
            };
            foreach(Item itm in EquippedItems.Values)
            {
                //Since equippedItems also contain weapons which do not have stat attributes, wrap in try 
                try
                {
                    Armor armor = (Armor)itm;
                    TotalAttributes[AttributeType.Dexterity] += armor.ArmourAttributes.Dexterity;
                    TotalAttributes[AttributeType.Strength] += armor.ArmourAttributes.Strength;
                    TotalAttributes[AttributeType.Intelligence] += armor.ArmourAttributes.Intelligence;
                }
                catch (InvalidCastException)
                {
                    //If invalidCastException then continue; since we arent interested in weapon dmg&attackspeed.
                    continue;
                }
            }
            return TotalAttributes;
        }
        /// <summary>
        /// Uses getTotalPrimaryAttribute to calculate base + gear attribute amount of the primary attribute for the selected class
        /// If weapon is equipped, reassign weaponDPS else leave as 1 if no weapon is equipped
        /// return calculated damage
        /// </summary>
        /// <returns></returns>
        public double DoDamage()
        {
            double weaponDPS = 1;
            int totalPrimaryAttr = GetTotalPrimaryAttributes()[PrimaryAttributeType];
            if (EquippedItems.ContainsKey(EquipmentSlot.Weapon))
            {
                Weapon wep = (Weapon)EquippedItems[EquipmentSlot.Weapon];
                weaponDPS = wep.WeaponAttributes.Damage * wep.WeaponAttributes.AttackSpeed;
            }
            double damage = weaponDPS * (1 + totalPrimaryAttr / 100);
            return damage;

        }
        /// <summary>
        /// Levels up the character by 1 for each call.
        /// Uses field set by child class constructor in order to set the correct attributes.
        /// </summary>~
        public void LevelUp()
        {
            Level++;
            BasePrimaryAttributes.Strength += OnLevelUpAttributeIncrease.Strength;
            BasePrimaryAttributes.Intelligence += OnLevelUpAttributeIncrease.Intelligence;
            BasePrimaryAttributes.Dexterity += OnLevelUpAttributeIncrease.Dexterity;

        }
    }
}