﻿using System;
using ConsoleApp2.Attributes;
using ConsoleApp2.enums;
using ConsoleApp2.SubClasses;
namespace ConsoleApp2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Warrior myWarr = new("gamer");
            Weapon toEquip = new()
            {
                Name = "Titanium Axe",
                Level = 2,
                EquipmentSlot = EquipmentSlot.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { AttackSpeed = 23.4, Damage = 1283 }
            };
            Armor armToEquip = new()
            {
                Name = "bodysuit of Gods",
                Level = 1,
                EquipmentSlot = EquipmentSlot.Body,
                ArmourType = ArmourType.Plate,
                ArmourAttributes = new BasePrimaryAttributes() { Strength = 1 }
            };
            myWarr.LevelUp();
            myWarr.EquipItem(toEquip);
            myWarr.EquipItem(armToEquip);
            myWarr.CharacterInfo();
        }
    }
}
