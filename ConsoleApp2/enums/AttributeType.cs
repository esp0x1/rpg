﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.enums
{
    // defines which attributes exist. Used to define primary(dmg increase) and to create dicts in Character
    public enum AttributeType
    {
        Strength,
        Intelligence,
        Dexterity,
    }
}
