﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.enums
{
    // defines which equipmentslots items are placed in, and type of slot a specific item uses.
    public enum EquipmentSlot
    {
        Head,
        Body,
        Legs,
        Weapon,
    }
}
