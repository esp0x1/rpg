﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.enums
{
    // Defines ArmourTypes, Also used to create allowed armour arrays
    public enum ArmourType
    {
        Cloth,
        Plate,
        Leather,
        Mail,
    }
}
