﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp2.BaseClasses;
using ConsoleApp2.SubClasses;

namespace ConsoleApp2.Interfaces
{
    public interface ICharacterActions
    { 
        public double DoDamage();
        public string EquipItem(Weapon weapon);
        public string EquipItem(Armor armor);
        public void LevelUp();
        public void CharacterInfo();
    }
}
