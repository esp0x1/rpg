﻿using ConsoleApp2.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Attributes
{
    public class BasePrimaryAttributes
    {
        public int Strength { get; set; }
        public int Intelligence { get; set; }
        public int Dexterity { get; set; }

        public BasePrimaryAttributes()
        {

        }
        public BasePrimaryAttributes(int strength, int intelligence, int dexterity)
        {
            Strength = strength;
            Intelligence = intelligence;
            Dexterity = dexterity;
        }
    }
}
