﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() : base(String.Format("Invalid Weapon Type for This Character Class"))
        {
                
        }
    }
}
