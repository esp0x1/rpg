﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Exceptions
{
    public class InvalidArmourException : Exception 
    {
        public InvalidArmourException() : base(String.Format("Invalid Armour for this character class"))
        {

        }
    }
}
