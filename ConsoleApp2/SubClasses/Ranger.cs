﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp2.BaseClasses;
using ConsoleApp2.enums;
using ConsoleApp2.Interfaces;
using ConsoleApp2.Attributes;
using ConsoleApp2.Exceptions;

namespace ConsoleApp2.SubClasses
{

    public class Ranger : Character
    {
        public const int InitialStrength = 1;
        public const int LevelUpStrength = 1;

        public const int InitialIntelligence = 1;
        public const int LevelUpIntelligence = 1;

        public const int InitialDexterity = 7;
        public const int LevelUpDexterity = 5;

        public Ranger(string name) : base(name)
        {
            AllowedArmour = new ArmourType[] { ArmourType.Leather, ArmourType.Mail };
            AllowedWeapons = new WeaponType[] { WeaponType.Bow };
            BasePrimaryAttributes = new BasePrimaryAttributes(InitialStrength, InitialIntelligence, InitialDexterity);
            OnLevelUpAttributeIncrease = new BasePrimaryAttributes(LevelUpStrength, LevelUpIntelligence, LevelUpDexterity);
            PrimaryAttributeType = AttributeType.Dexterity;

        }
    }
}
