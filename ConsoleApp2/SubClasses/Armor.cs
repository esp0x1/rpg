﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp2.BaseClasses;
using ConsoleApp2.Attributes;
using ConsoleApp2.enums;
namespace ConsoleApp2.SubClasses
{
    public class Armor : Item
    {
        public BasePrimaryAttributes ArmourAttributes { get; set; }
        public ArmourType ArmourType { get; set; }

        public Armor() : base()
        {

        }
        public Armor(BasePrimaryAttributes armourAttributes, ArmourType armourType, string name, int level, EquipmentSlot equipmentSlot) : base(name,level,equipmentSlot)
        {
            ArmourAttributes = armourAttributes;
            ArmourType = armourType;
        }

    }
}
