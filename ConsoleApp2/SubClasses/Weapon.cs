﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp2.BaseClasses;
using ConsoleApp2.Attributes;
using ConsoleApp2.enums;
namespace ConsoleApp2.SubClasses
{
    public class Weapon : Item
    {
        public WeaponAttributes WeaponAttributes { get; set; }
        public WeaponType WeaponType { get; set; }
        public Weapon() : base()
        {

        }
        public Weapon(WeaponAttributes weaponAttributes, WeaponType weaponType, string name, int level, EquipmentSlot equipmentSlot) : base(name, level, equipmentSlot)
        {
            WeaponAttributes = weaponAttributes;
            WeaponType = weaponType;
        }
    }
}
