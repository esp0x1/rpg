﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp2.BaseClasses;
using ConsoleApp2.enums;
using ConsoleApp2.Interfaces;
using ConsoleApp2.Attributes;
using ConsoleApp2.Exceptions;

namespace ConsoleApp2.SubClasses
{

    public class Warrior : Character
    {
        //Initial and levelup increase attribute values
        public const int InitialStrength = 5;
        public const int LevelUpStrength = 3;

        public const int InitialIntelligence = 1;
        public const int LevelUpIntelligence = 1;

        public const int InitialDexterity = 2;
        public const int LevelUpDexterity = 2;

        public Warrior(string name) : base(name)
        {
            AllowedArmour = new ArmourType[] { ArmourType.Mail, ArmourType.Plate };
            AllowedWeapons = new WeaponType[] { WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword };
            BasePrimaryAttributes = new BasePrimaryAttributes(InitialStrength, InitialIntelligence, InitialDexterity);
            OnLevelUpAttributeIncrease = new BasePrimaryAttributes(LevelUpStrength, LevelUpIntelligence, LevelUpDexterity);
            PrimaryAttributeType = AttributeType.Strength;

        }
    }
}
