﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp2.BaseClasses;
using ConsoleApp2.enums;
using ConsoleApp2.Interfaces;
using ConsoleApp2.Attributes;
using ConsoleApp2.Exceptions;

namespace ConsoleApp2.SubClasses
{

    public class Mage : Character
    {
        public const int InitialStrength = 1;
        public const int LevelUpStrength = 1;

        public const int InitialIntelligence = 8;
        public const int LevelUpIntelligence = 5;

        public const int InitialDexterity = 1;
        public const int LevelUpDexterity = 1;
        
        public Mage(string name) : base(name)
        {
            AllowedArmour = new ArmourType[] { ArmourType.Cloth };
            AllowedWeapons = new WeaponType[] { WeaponType.Staff, WeaponType.Wand };
            BasePrimaryAttributes = new BasePrimaryAttributes(InitialStrength, InitialIntelligence, InitialDexterity);
            OnLevelUpAttributeIncrease = new BasePrimaryAttributes(LevelUpStrength, LevelUpIntelligence, LevelUpStrength);
            PrimaryAttributeType = AttributeType.Intelligence;

        }
    }
}
