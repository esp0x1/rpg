﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp2.BaseClasses;
using ConsoleApp2.enums;
using ConsoleApp2.Interfaces;
using ConsoleApp2.Attributes;
using ConsoleApp2.Exceptions;

namespace ConsoleApp2.SubClasses
{

    public class Rogue : Character
    {
        public const int InitialStrength = 2;
        public const int LevelUpStrength = 1;

        public const int InitialIntelligence = 1;
        public const int LevelUpIntelligence = 1;

        public const int InitialDexterity = 6;
        public const int LevelUpDexterity = 4;

        public Rogue(string name) : base(name)
        {
            AllowedArmour = new ArmourType[] { ArmourType.Mail, ArmourType.Leather };
            AllowedWeapons = new WeaponType[] { WeaponType.Dagger, WeaponType.Sword };
            BasePrimaryAttributes = new BasePrimaryAttributes(InitialStrength, InitialIntelligence, InitialDexterity);
            OnLevelUpAttributeIncrease = new BasePrimaryAttributes(LevelUpStrength, LevelUpIntelligence, LevelUpDexterity);
            PrimaryAttributeType = AttributeType.Dexterity;
        }
    }
}
