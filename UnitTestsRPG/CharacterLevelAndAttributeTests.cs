﻿using System;
using Xunit;
using ConsoleApp2.SubClasses;
using ConsoleApp2.Attributes;

namespace UnitTestsRPG
{
    public class CharacterLevelAndAttributeTests
    {
        [Fact]
        public void Instantiate_NewCharacter_ShouldBeLevelOne()
        {
            // Arrange
            int expected = 1;
            Mage myCharacter = new Mage("Frodo");
            // Act
            int actual = myCharacter.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_NewCharacter_ShouldBeLevelTwo()
        {
            // Arrange
            int expected = 2;
            Mage myCharacter = new Mage("Frodo");
            // Act
            myCharacter.LevelUp();
            int actual = myCharacter.Level;
            //Assert
            Assert.Equal(expected, actual);
        }
        // MAGE
        [Fact]
        public void Instantiate_NewMage_AttributesShouldBe1Strength8Intelligence1Dexterity()
        {
            // Arrange
            //BasePrimaryAttributes expected = new BasePrimaryAttributes(1, 8, 1);
            int[] expected = new int[] { 1, 8, 1 };
            Mage myMage = new Mage("Frodo");
            //Act
            int[] actual = new int[] { myMage.BasePrimaryAttributes.Strength, myMage.BasePrimaryAttributes.Intelligence, myMage.BasePrimaryAttributes.Dexterity };
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_MageFromLevel1To2_AttributesShouldBe2Strength8Intelligence2Dexterity()
        {
            // Arrange
            //BasePrimaryAttributes expected = new BasePrimaryAttributes(2, 13, 2);
            int[] expected = new int[] { 2, 13, 2 };
            Mage myMage = new Mage("Frodo");
            // Act
            myMage.LevelUp();
            int[] actual = new int[] { myMage.BasePrimaryAttributes.Strength, myMage.BasePrimaryAttributes.Intelligence, myMage.BasePrimaryAttributes.Dexterity };
            //Assert
            Assert.Equal(expected, actual);
        }   
        
        // WARRIOR
        [Fact]
        public void Instantiate_NewWarrior_AttributesShouldBe5Strength1Intelligence2Dexterity()
        {
            // Arrange
            //BasePrimaryAttributes expected = new BasePrimaryAttributes(1, 8, 1);
            int[] expected = new int[] { 5, 1, 2 };
            Warrior myWarrior = new Warrior("Peter");
            //Act
            int[] actual = new int[] { myWarrior.BasePrimaryAttributes.Strength, myWarrior.BasePrimaryAttributes.Intelligence, myWarrior.BasePrimaryAttributes.Dexterity };
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_WarriorFromLevel1To2_AttributesShouldBe8Strength2Intelligence4Dexterity()
        {
            // Arrange
            //BasePrimaryAttributes expected = new BasePrimaryAttributes(2, 13, 2);
            int[] expected = new int[] { 8, 2, 4 };
            Warrior myWarrior = new Warrior("Peter");
            // Act
            myWarrior.LevelUp();
            int[] actual = new int[] { myWarrior.BasePrimaryAttributes.Strength, myWarrior.BasePrimaryAttributes.Intelligence, myWarrior.BasePrimaryAttributes.Dexterity };
            //Assert
            Assert.Equal(expected, actual);
        }        
        //Ranger
        [Fact]
        public void Instantiate_NewRanger_AttributesShouldBe1Strength1Intelligence7Dexterity()
        {
            // Arrange
            int[] expected = new int[] { 1, 1, 7 };
            Ranger myRanger = new Ranger("Peter");
            //Act
            int[] actual = new int[] { myRanger.BasePrimaryAttributes.Strength, myRanger.BasePrimaryAttributes.Intelligence, myRanger.BasePrimaryAttributes.Dexterity };
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_RangerFromLevel1To2_AttributesShouldBe2Strength2Intelligence12Dexterity()
        {
            // Arrange
            int[] expected = new int[] { 2, 2, 12 };
            Ranger myRanger = new Ranger("Peter");
            // Act
            myRanger.LevelUp();
            int[] actual = new int[] { myRanger.BasePrimaryAttributes.Strength, myRanger.BasePrimaryAttributes.Intelligence, myRanger.BasePrimaryAttributes.Dexterity };
            //Assert
            Assert.Equal(expected, actual);
        }        
        // Rogue
        [Fact]
        public void Instantiate_NewRogue_AttributesShouldBe2Strength1Intelligence6Dexterity()
        {
            // Arrange
            int[] expected = new int[] { 2, 1, 6 };
            Rogue myRogue = new Rogue("Peter");
            //Act
            int[] actual = new int[] { myRogue.BasePrimaryAttributes.Strength, myRogue.BasePrimaryAttributes.Intelligence, myRogue.BasePrimaryAttributes.Dexterity };
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_RogueFromLevel1To2_AttributesShouldBe3Strength2Intelligence10Dexterity()
        {
            // Arrange
            int[] expected = new int[] { 3, 2, 10 };
            Rogue myRogue = new Rogue("Peter");
            // Act
            myRogue.LevelUp();
            int[] actual = new int[] { myRogue.BasePrimaryAttributes.Strength, myRogue.BasePrimaryAttributes.Intelligence, myRogue.BasePrimaryAttributes.Dexterity };
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
