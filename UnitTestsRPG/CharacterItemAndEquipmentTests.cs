﻿using System;
using Xunit;
using ConsoleApp2.SubClasses;
using ConsoleApp2.Attributes;
using ConsoleApp2.enums;
using ConsoleApp2.Exceptions;
using static System.Net.Mime.MediaTypeNames;

namespace UnitTestsRPG
{
    public class CharacterItemAndEquipmentTests
    {
        [Fact]
        public void EquipItem_Level2AxeOnLevel1Warrior_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Weapon expected = new()
            {
                Name = "Awful But Great Looking Axe",
                Level = 2,
                EquipmentSlot = EquipmentSlot.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { AttackSpeed = 1.1, Damage = 1 }
            };
            Warrior myWarrior = new Warrior("GiantWarrior1337");
            // Act
            Action actual  = () => myWarrior.EquipItem(expected);
            // Assert
            Assert.Throws<InvalidWeaponException>(actual);
        }
        [Fact]
        public void EquipItem_Level2PlateBodyOnLevel1Warrior_ShouldThrowInvalidArmorException()
        {
            // Arrange
            Armor expected = new()
            {
                Name = "Heavy Bodysuit of plate",
                Level = 2,
                EquipmentSlot = EquipmentSlot.Body,
                ArmourType = ArmourType.Plate,
                ArmourAttributes = new BasePrimaryAttributes() { Strength = 2 }
            };
            Warrior myWarrior = new Warrior("GiantWarrior1337");
            // Act
            Action actual = () => myWarrior.EquipItem(expected);
            // Assert
            Assert.Throws<InvalidArmourException>(actual);
        }
        [Fact]
        public void EquipItem_BowOnLevel1Warrior_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Weapon expected = new()
            {
                Name = "Twig Bow",
                Level = 1,
                EquipmentSlot = EquipmentSlot.Weapon,
                WeaponType = WeaponType.Bow,
                WeaponAttributes = new WeaponAttributes() { AttackSpeed = 1.1, Damage = 1 }
            };
            Warrior myWarrior = new Warrior("GiantWarrior1337");
            // Act
            Action actual = () => myWarrior.EquipItem(expected);
            // Assert
            Assert.Throws<InvalidWeaponException>(actual);
        }
        [Fact]
        public void EquipItem_ClothHeadOnWarrior_ShouldThrowInvalidArmorException()
        {
            // Arrange
            Armor expected = new()
            {
                Name = "Magic Cloth head of cloth",
                Level = 1,
                EquipmentSlot = EquipmentSlot.Head,
                ArmourType = ArmourType.Cloth,
                ArmourAttributes = new BasePrimaryAttributes() { Intelligence = 1, Dexterity = 12 }
            };
            Warrior myWarrior = new Warrior("TheTrashman928");
            // Act
            Action actual = () => myWarrior.EquipItem(expected);
            // Assert
            Assert.Throws<InvalidArmourException>(actual);
        }
        [Fact]
        public void EquipItem_Level1AxeOnLevel1Warrior_ShouldReturnSuccessMessage()
        {
            // Arrange
            Weapon toEquip = new()
            {
                Name = "Titanium Axe",
                Level = 1,
                EquipmentSlot = EquipmentSlot.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { AttackSpeed = 1.1, Damage = 129 }
            };
            Warrior myWarrior = new Warrior("Doofus92");
            string expected = "New Weapon Equipped!";
            // Act
            string actual = myWarrior.EquipItem(toEquip);
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipItem_Level1PlateHelmetOnLevel1Warrior_ShouldReturnSuccessMessage()
        {
            // Arrange
            Armor toEquip = new()
            {
                Name = "Plated Helm",
                Level = 1,
                EquipmentSlot = EquipmentSlot.Head,
                ArmourType = ArmourType.Plate,
                ArmourAttributes = new BasePrimaryAttributes() { Strength = 4  }
            };
            Warrior myWarrior = new Warrior("goofballer35");
            string expected = "New Armour Equipped!";
            // Act
            string actual = myWarrior.EquipItem(toEquip);
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void DoDamage_Level1WarriorWithoutWeaponEquipped_ShouldReturn1()
        {
            // Arrange
            double expected = 1 * (1 + (5 / 100));
            Warrior myWarrior = new Warrior("nuclearScientistWarrior92");
            // Act
            double actual = myWarrior.DoDamage();
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void DoDamage_Level1WarriorLevel1AxeEquipped_ShouldReturn7()
        {
            // Arrange
            double expected = (7 * 1.1) * (1 + (5 / 100));
            Warrior myWarrior = new Warrior("VideoGamer02");
            Weapon toEquip = new()
            {
                Name = "Titanium Axe",
                Level = 1,
                EquipmentSlot = EquipmentSlot.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { AttackSpeed = 1.1, Damage = 7 }
            };
            // Act
            myWarrior.EquipItem(toEquip);
            double actual = myWarrior.DoDamage();
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void DoDamage_Level1WarriorLevel1AxeAndLevel1PlateBodyEquipped_ShouldReturn8()
        {
            // Arrange
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));
            Warrior myWarrior = new Warrior("VideoGamer02");
            Weapon wepToEquip = new()
            {
                Name = "Titanium Axe",
                Level = 1,
                EquipmentSlot = EquipmentSlot.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { AttackSpeed = 1.1, Damage = 7 }
            };            
            Armor armToEquip = new()
            {
                Name = "bodysuit of Gods",
                Level = 1,
                EquipmentSlot = EquipmentSlot.Body,
                ArmourType = ArmourType.Plate,
                ArmourAttributes = new BasePrimaryAttributes() { Strength=1 }
            };
            // Act
            myWarrior.EquipItem(wepToEquip);
            myWarrior.EquipItem(armToEquip);
            double actual = myWarrior.DoDamage();
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
